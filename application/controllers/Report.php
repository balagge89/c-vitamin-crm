<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller {
   
    public function index() {
        $this->load->view('report_view');
    }

    public function query() {
        
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $start_time = $start_date."T00:00:01";
        $end_time = $end_date."T23:59:59";

        $datas = array();
        $organizations = $this->db->query("SELECT name, ac_id FROM organizations WHERE report = '1'");
        $organizations->result_array();
       
        foreach ($organizations->result_array() as $organization_name) {
            $organization = $organization_name['name'];
            $organization_id = $organization_name['ac_id'];
            
            //Összes Task Lekérés
            $all_tasks = $this->db->query("SELECT COUNT(id) AS alltask FROM tasks WHERE organization = '$organization' AND created_date BETWEEN '$start_time' AND '$end_time'");
            $all_tasks->result();

            //Sajár Maga Által Generált Task Lekérés
            $own_tasks = $this->db->query("SELECT COUNT(id) AS owntask FROM tasks WHERE automation_id IS NULL AND organization = '$organization' AND created_date BETWEEN '$start_time' AND '$end_time'");
            $own_tasks->result();

            //Összes Elvégzett Task
            $done_tasks = $this->db->query("SELECT COUNT(id) AS done_task FROM tasks WHERE status = '1' AND organization = '$organization' AND done_date BETWEEN '$start_time' AND '$end_time'");
            $done_tasks->result();

            //Összes Elvégzett Task Automata Nélkül
            $done_tasks_own = $this->db->query("SELECT COUNT(id) AS done_task_own FROM tasks WHERE automation_id IS NULL AND status = '1' AND organization = '$organization' AND done_date BETWEEN '$start_time' AND '$end_time'");
            $done_tasks_own->result();

            //Hívás Taskok
            $call_tasks = $this->db->query("SELECT COUNT(id) AS call_task FROM tasks WHERE task_type = '1' AND organization = '$organization' AND created_date BETWEEN '$start_time' AND '$end_date'");
            $call_tasks->result();

            //Hívás Taskok
            $call_tasks_done = $this->db->query("SELECT COUNT(id) AS call_task_done FROM tasks WHERE status = '1' AND task_type = '1' AND organization = '$organization' AND done_date BETWEEN '$start_time' AND '$end_time'");
            $call_tasks_done->result();
            
            //Érdemben elérve átlagosan ennyi nap alatt
            $reach_date_diff_avg = $this->db->query("SELECT ROUND(AVG(users.reach_date_diff)) AS reach_date_diff_avg FROM users WHERE users.reach_organization = '$organization' AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $reach_date_diff_avg->result();
            
            //Prosi átlagos kiküldése
            $brochure_date_diff_avg = $this->db->query("SELECT ROUND(AVG(users.brochure_date_diff)) AS brochure_date_diff_avg FROM users WHERE users.organization = '$organization_id' AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $brochure_date_diff_avg->result();

            //Prosi 15 napon belül kiküldve
            $brochure_sent_15_days = $this->db->query("SELECT COUNT(users.brochure_date_diff) AS brochures_number FROM users WHERE brochure_date_diff < '16' AND users.organization = '$organization_id' AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $brochure_sent_15_days->result();

            //Ezen időszak alatt beérkezett összes lead
            $all_users = $this->db->query("SELECT COUNT(users.user_id) AS all_users FROM users WHERE users.organization = '$organization_id' AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $all_users->result();

            //Open Páciensek
            $open_status_patient = $this->db->query("SELECT COUNT(user_id) AS open_patients FROM users WHERE users.organization = '$organization_id' AND deal_status = '0' AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $open_status_patient->result();

            //Open Páciensek Várossal
            $open_status_patient_with_town = $this->db->query("SELECT COUNT(user_id) AS open_patients FROM users WHERE users.organization = '$organization_id' AND deal_status = '0' AND town IS NOT NULL AND users.created_date BETWEEN '$start_time' AND '$end_time'");
            $open_status_patient_with_town->result();

            if ($brochure_sent_15_days->row('brochures_number') > 0) {
                $datas['brochure_sent_15_days_percent'][] = round($brochure_sent_15_days->row('brochures_number') / $all_users->row('all_users')*100);
            }
            else {
                $datas['brochure_sent_15_days_percent'][] = 0;
            }

            //Listázandó TAGEK lekérése
            $user_tag_ids = $this->db->query("SELECT tag_id FROM tags WHERE report = '1'");
            $user_tag_ids->result_array();
            $user_tag_list = array();
            foreach ($user_tag_ids->result_array() as $user_tag_id) {
                $user_tag_list[] = $user_tag_id['tag_id'];
            }
            
            //TAGEK összámolása
            $user_tags = $this->db->query("SELECT COUNT(user_tags.user_id) AS users, user_tags.tag_id, tags.tag FROM user_tags LEFT JOIN tags ON user_tags.tag_id = tags.tag_id WHERE user_tags.organization = '$organization_id' AND user_tags.created_date BETWEEN '$start_time' AND '$end_time' AND user_tags.tag_id in (" . implode(",", array_map("intval", $user_tag_list)) . ") GROUP BY user_tags.tag_id ORDER BY user_tags.tag_id");
            $user_tags->result();

             //Listázandó LISTÁK lekérése
             $user_list_ids = $this->db->query("SELECT list_id FROM lists WHERE report = '1'");
             $user_list_ids->result_array();
             $user_lists_list = array();
             foreach ($user_list_ids->result_array() as $user_list_id) {
                 $user_lists_list[] = $user_list_id['list_id'];
             }
             
             //LISTÁK összámolása
             //echo ("SELECT COUNT(user_lists.user_id) AS users, user_lists.list_id, lists.list FROM user_lists LEFT JOIN lists ON user_lists.list_id = lists.list_id WHERE user_lists.organization = '$organization_id' AND user_lists.created_date BETWEEN '$start_time' AND '$end_time' AND user_lists.list_id in (" . implode(",", array_map("intval", $user_lists_list)) . ") GROUP BY user_lists.list_id ORDER BY user_lists.list_id")."<br>";
             $user_lists = $this->db->query("SELECT COUNT(user_lists.user_id) AS users, user_lists.list_id, lists.list FROM user_lists LEFT JOIN lists ON user_lists.list_id = lists.list_id WHERE user_lists.organization = '$organization_id' AND user_lists.created_date BETWEEN '$start_time' AND '$end_time' AND user_lists.list_id in (" . implode(",", array_map("intval", $user_lists_list)) . ") GROUP BY user_lists.list_id ORDER BY user_lists.list_id");
             $user_lists->result();
                  

            $datas['organization'][] = $organization;
            $datas['alltask'][] = $all_tasks->row('alltask');         
            $datas['owntask'][] = $own_tasks->row('owntask');
            $datas['done_task'][] = $done_tasks->row('done_task');
            $datas['done_task_own'][] = $done_tasks_own->row('done_task_own');
            $datas['call_task'][] = $call_tasks->row('call_task');
            $datas['call_task_done'][] = $call_tasks_done->row('call_task_done');
            $datas['brochure_date_diff_avg'][] = $brochure_date_diff_avg->row('brochure_date_diff_avg');
            $datas['reach_date_diff_avg'][] = $reach_date_diff_avg->row('reach_date_diff_avg');
            $datas['open_status_patient'][] = $open_status_patient->row('open_patients');
            $datas['open_status_patient_with_town'][] = $open_status_patient_with_town->row('open_patients');
            $datas['user_tags_data'][] = $user_tags->result();
            $datas['user_lists_data'][] = $user_lists->result();
            
            
            
        }
        $datas['end_date'] = $end_date;
        $datas['start_date'] = $start_date;
        $this->load->view('report_view', $datas);
    }

}
