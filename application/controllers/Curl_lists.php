<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_lists extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";

        //Find Last TAG ID
        $this->db->select('list_id');
        $this->db->order_by('list_id', 'ASC');
        $query = $this->db->get('lists', 1);
        
        foreach ($query->result() as $row) {
            $last_list = $row->list_id;
        }

        $start_list_id = $last_list - 1;
        $end_list_id = $last_list - 20;

        //Start Looping TAGS
        for ($x = $start_list_id; $x >= $end_list_id; $x--) {

            $list_id = $x;
            $data['list_id'] = $list_id;

            //Retrieve Tag Data From ActiveCampaign API
            $lists = "https://cvitaminclinic.api-us1.com/api/3/lists/$list_id&api_key=$api_key";

            $lists_buffer = @file_get_contents($lists);

            $data['lists'] = json_decode($lists_buffer);


            if ( (!empty($data['lists'])) && (!empty($data['lists'])) ) {
   
                foreach ($data['lists'] as $list) {
                    echo $list->id."<br>";
                    echo $list->name."<br>";
                    echo "<br>";
                   

                    //Compile Data to Insert Database
                    $db_data = array(
                        'list_id' => $list_id,
                        'list' => $list->name
                    );

                    $this->db->insert('lists', $db_data);
                    
                }

            }

        }
    }
}
