<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_user_lists extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";

        //Find Last TAG ID
        $this->db->select('user_id');
        $this->db->order_by('user_id', 'DESC');
        $query = $this->db->get('user_lists', 1);
        
        foreach ($query->result() as $row) {
            $last_user = $row->user_id;
        }

        $start_user_id = $last_user + 1;
        $end_user_id = $last_user + 20;

        //Start Looping USERS
        for ($x = $start_user_id; $x <= $end_user_id; $x++) {

            $user_id = $x;
            $data['user_id'] = $user_id;

     
            //Retrieve List Data From ActiveCampaign API
            $user_lists = "https://cvitaminclinic.api-us1.com/api/3/contacts/$user_id/contactLists&api_key=$api_key";

            $user_lists_buffer = @file_get_contents($user_lists);

            $data['user_lists'] = json_decode($user_lists_buffer);


            if ( (!empty($data['user_lists'])) && (!empty($data['user_lists'])) ) {
   
                foreach ($data['user_lists']->contactLists as $user_list) {
                    echo $user_list->contact."<br>";
                    echo $user_list->sdate."<br>";
                    echo $user_list->list."<br>";
                    
                    
                    //Fetch Organization ID from local DB
                    $this->db->select('organization');
                    $this->db->where('user_id', $user_list->contact);
                    $organization_query = $this->db->get('users');
                    
                    foreach ($organization_query->result() as $organization_id)
                    {
                        echo $organization_id->organization."<br>";
                    }
                    echo "<br>";
                    
                    
                    //Compile Data to Insert Database
                    $db_data = array(
                        'user_id' => $user_list->contact,
                        'created_date' => $user_list->sdate,
                        'list_id' => $user_list->list,
                        'organization' => $organization_id->organization                
                    );

                    $this->db->insert('user_lists', $db_data);
                    
                }

            }

        }
    }
}
