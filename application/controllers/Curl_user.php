<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_user extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";
        
        function day_calc($first_date, $second_date) {
            $date1 = new DateTime($first_date);
            $date2 = new DateTime($second_date);
            $days  = $date2->diff($date1)->format('%a')+1;
            return $days;
        }


        $data['user'] = array();
        
            $user_id = $_POST['contact']['id'];
            $this->db->delete('users', array('user_id' => $user_id)); 

            //Retrieve User Data From ActiveCampaign API
            $users = "https://cvitaminclinic.api-us1.com/api/3/contacts/$user_id?api_key=$api_key";

            $users_buffer = @file_get_contents($users);

            $data['users'] = json_decode($users_buffer);
            

            if ( !empty($data['users']) ) {

                $user_datas = array();
                array_push($user_datas, $data['users']);
                
                //Retrieve User INFOS
                foreach ($user_datas as $user_info) {
                    //Undifined Index Error Prevention
                    $data['organization'][$x] = NULL;
                    $data['brochure_date'][$x] = NULL;
                    $data['brochure_date_diff'][$x] = NULL;
                    $data['brochure_organization'][$x] = NULL;
                    $data['reach_date'][$x] = NULL;
                    $data['reach_date_diff'][$x] = NULL;
                    $data['reach_organization'][$x] = NULL;
                    $data['town'][$x] = NULL;
                    $data['deal_status'][$x] = NULL;
                   

                    $data['user_id'][$x] = $user_id;
                    $data['created_date'][$x] = $user_info->contact->cdate;
                    $data['email'][$x] = $user_info->contact->email;
                    $data['user_name'][$x] = $user_info->contact->firstName." ".$user_info->contact->lastName;
                    if (isset($user_info->deals[0]->organization)) {
                        $data['organization'][$x] = $user_info->deals[0]->organization;
                    }
                    if (isset($user_info->deals[0]->status)) {
                        $data['deal_status'][$x] = $user_info->deals[0]->status;
                    }
                    
                    
                    //Find certain field datas
                    foreach ($user_info->fieldValues as $field_data) {
                       
                        if ($field_data->field == 134) {
                            $data['brochure_date'][$x] = $field_data->value;
                            $data['brochure_date_diff'][$x] = day_calc($user_info->contact->cdate,$field_data->value);
                        }
                        elseif ($field_data->field == 155) {
                            $data['brochure_organization'][$x] = $field_data->value;            
                        }
                        elseif ($field_data->field == 154) {
                            $data['reach_date'][$x] = $field_data->value;
                            $data['reach_date_diff'][$x] = day_calc($user_info->contact->cdate,$field_data->value);           
                        }
                        elseif ($field_data->field == 156) {
                            $data['reach_organization'][$x] = $field_data->value; 
                        }
                        elseif ($field_data->field == 20) {
                            $data['town'][$x] = $field_data->value; 
                        }

                    }
                    
                    //Compile Data to Insert Database
                    $db_data = array(
                        'user_id' => $data['user_id'][$x],
                        'created_date' =>  $data['created_date'][$x],
                        'email' => $data['email'][$x],
                        'user_name' => $data['user_name'][$x],
                        'town' => $data['town'][$x],
                        'organization' => $data['organization'][$x],                       
                        'brochure_date' => $data['brochure_date'][$x],
                        'brochure_date_diff' => $data['brochure_date_diff'][$x],
                        'brochure_organization' => $data['brochure_organization'][$x],
                        'reach_date' => $data['reach_date'][$x],
                        'reach_date_diff' => $data['reach_date_diff'][$x],
                        'reach_organization' => $data['reach_organization'][$x],  
                        'deal_status' => $data['deal_status'][$x]  

                    );

                    $this->db->insert('users', $db_data);
                }
         
            }        

    } 
}
