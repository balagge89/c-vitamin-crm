<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_report extends CI_Controller {
   
    public function index() {
        $this->load->view('user_report_view');
    }

    public function query() {
        
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $start_time = $start_date."T00:00:01";
        $end_time = $end_date."T23:59:59";

        $datas = array();
        $organizations = $this->db->query("SELECT name FROM organizations WHERE report = '1'");
        $organizations->result_array();
       
        foreach ($organizations->result_array() as $organization_name) {
            $organization = $organization_name['name'];
            
            //Összes Task Lekérés
            $all_tasks = $this->db->query("SELECT COUNT(id) AS alltask FROM tasks WHERE organization = '$organization' AND created_date BETWEEN '$start_time' AND '$end_time'");
            $all_tasks->result();

        
            $datas['organization'][] = $organization;
            $datas['alltask'][] = $all_tasks->row('alltask');         
      
            
        }
        
        $datas['end_date'] = $end_date;
        $datas['start_date'] = $start_date;
        $this->load->view('user_report_view', $datas);
    }

}
