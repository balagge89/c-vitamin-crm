<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_tags extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";

        //Find Last TAG ID
        $this->db->select('tag_id');
        $this->db->order_by('tag_id', 'ASC');
        $query = $this->db->get('tags', 1);
        
        foreach ($query->result() as $row) {
            $last_tag = $row->tag_id;
        }

        $start_tag_id = $last_tag - 1;
        $end_tag_id = $last_tag - 20;

        //Start Looping TAGS
        for ($x = $start_tag_id; $x >= $end_tag_id; $x--) {

            $tag_id = $x;
            $data['tag_id'] = $tag_id;

            //Record TAG ID to Database
            $tags2_data = array(
                'tag_id' => $tag_id,
            );
            //$this->db->insert('tags', $tags2_data);

            //Retrieve Tag Data From ActiveCampaign API
            $tags = "https://cvitaminclinic.api-us1.com/api/3/tags/$tag_id&api_key=$api_key";

            $tags_buffer = @file_get_contents($tags);

            $data['tags'] = json_decode($tags_buffer);


            if ( (!empty($data['tags'])) && (!empty($data['tags'])) ) {
   
                foreach ($data['tags'] as $tag) {
                    echo $tag->id."<br>";
                    echo $tag->tag."<br>";
                    echo "<br>";
                   

                    //Compile Data to Insert Database
                    $db_data = array(
                        'tag_id' => $tag_id,
                        'tag' => $tag->tag
                    );

                    $this->db->insert('tags', $db_data);
                    
                }

            }

        }
    }
}
