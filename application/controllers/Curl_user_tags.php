<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_user_tags extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";

        //Find Last TAG ID
        $this->db->select('user_id');
        $this->db->order_by('user_id', 'DESC');
        $query = $this->db->get('user_tags', 1);
        
        foreach ($query->result() as $row) {
            $last_user = $row->user_id;
        }

        $start_user_id = $last_user + 1;
        $end_user_id = $last_user + 20;

        //Start Looping USERS
        for ($x = $start_user_id; $x <= $end_user_id; $x++) {

            $user_id = $x;
            $data['user_id'] = $user_id;

     
            //Retrieve Tag Data From ActiveCampaign API
            $user_tags = "https://cvitaminclinic.api-us1.com/api/3/contacts/$user_id/contactTags&api_key=$api_key";

            $user_tags_buffer = @file_get_contents($user_tags);

            $data['user_tags'] = json_decode($user_tags_buffer);


            if ( (!empty($data['user_tags'])) && (!empty($data['user_tags'])) ) {
   
                foreach ($data['user_tags']->contactTags as $user_tag) {
                    echo $user_tag->contact."<br>";
                    echo $user_tag->cdate."<br>";
                    echo $user_tag->tag."<br>";
                    

                    //Fetch Organization ID from local DB
                    $this->db->select('organization');
                    $this->db->where('user_id', $user_tag->contact);
                    $organization_query = $this->db->get('users');
                    
                    foreach ($organization_query->result() as $organization_id)
                    {
                        echo $organization_id->organization."<br>";
                    }
                    echo "<br>";
                    
                    
                    //Compile Data to Insert Database
                    $db_data = array(
                        'user_id' => $user_tag->contact,
                        'created_date' => $user_tag->cdate,
                        'tag_id' => $user_tag->tag,
                        'organization' => $organization_id->organization                
                    );

                    $this->db->insert('user_tags', $db_data);
                    
                }

            }

        }
    }
}
