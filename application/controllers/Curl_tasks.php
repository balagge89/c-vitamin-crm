<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Curl_tasks extends CI_Controller
{

    public function index()
    {

        $api_key = "cd060831ba275ed01ada5a30a670ec34c418db622f09019b40d8248ebdb32bff8bdfe11b";

        //Find Last Deal ID
        $this->db->select('deal_id');
        $this->db->order_by('deal_id', 'ASC');
        $query = $this->db->get('deals', 1);
        
        foreach ($query->result() as $row) {
            $last_deal = $row->deal_id;
        }

        $start_deal_id = $last_deal - 1;
        $end_deal_id = $last_deal - 20;

        //Start Looping Deals
        for ($x = $start_deal_id; $x >= $end_deal_id; $x--) {

            $deal_id = $x;
            $data['deal_id'] = $deal_id;

            //Record Deal ID to Database
            $deals2_data = array(
                'deal_id' => $deal_id,
            );
            $this->db->insert('deals', $deals2_data);

            //Retrieve Deal & Task Data From ActiveCampaign API
            $dealtasks = "https://cvitaminclinic.api-us1.com/api/3/dealTasks&api_key=$api_key&filters%5Brelid%5D=$deal_id";
            $deals = "https://cvitaminclinic.api-us1.com/api/3/deals/$deal_id&api_key=$api_key";

            $tasks_buffer = @file_get_contents($dealtasks);
            $deals_buffer = @file_get_contents($deals);

            $data['deals'] = json_decode($deals_buffer);
            $data['tasks'] = json_decode($tasks_buffer);

            if ( (!empty($data['tasks'])) && (!empty($data['deals'])) ) {

                foreach ($data['tasks']->dealTasks as $task) {
                    foreach ($data['deals'] as $deal) {

                        //Retrieve Organization ID From ActiveCampaign API
                        $task_id = $task->id;
                        $organization = "https://cvitaminclinic.api-us1.com/api/3/dealTasks/$task_id/owner?api_key=$api_key";
                        $organization_buffer = @file_get_contents($organization);
                        $data['organizations'] = json_decode($organization_buffer);

                        //Find the owners name in the database
                        foreach ($data['organizations'] as $organization) {
                                //If there is no owner ID we create blank
                               if (isset($organization->owner)) {
                                    echo "";
                                    $organization_id = $organization->owner;
                                }
                                else {
                                    $organization_id = 111;
                                }

                                $organization_name = $this->db->query("SELECT name FROM organizations WHERE owner_id = $organization_id");
                                $organization_name->result();
                                
                        };
                        
                        $data['organizations'] = $organization_name->row('name');
                    }

                    //Compile Data to Insert Database
                    $db_data = array(
                        'deal_id' => $deal_id,
                        'task_name' => $task->title,
                        'task_id' => $task->id,
                        'note' => $task->note,
                        'last_mod' => $task->udate,
                        'created_date' => $task->cdate,
                        'done_date' => @$task->donedate,
                        'notification_date' => $task->duedate,
                        'deadline' => $task->edate,
                        'status' => $task->status,
                        'organization' => $organization_name->row('name'),
                        'organization_id' => $deal->owner,
                        'automation_id' => $task->automation,
                        'task_type' => $task->dealTasktype
                    );

                    $this->db->insert('tasks', $db_data);
                }

            }

            $this->load->view('dealtasks', $data);

        }
    }
}
