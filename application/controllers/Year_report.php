<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Year_report extends CI_Controller {
   
    public function index() {
        
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $datas = array();
        $organizations = $this->db->query("SELECT * FROM organizations WHERE report = '1' ORDER BY id ASC");
        
        $organizations->result_array();
        
        foreach ($organizations->result_array() as $organization_name) {
            for ($i = 1; $i <= 12; $i++) {

                $lastDayThisMonth = cal_days_in_month(CAL_GREGORIAN,$i,2018);

                $organization = $organization_name['name'];
                $fomatted_i = sprintf('%02d', $i);
                //Összes Task Lekérés
                $all_tasks = $this->db->query("SELECT COUNT(id) AS alltask FROM tasks WHERE organization = '$organization' AND created_date BETWEEN '2018-$fomatted_i-01 00:00:01' AND '2018-$fomatted_i-$lastDayThisMonth 23:59:01' ORDER BY organization ASC");
                $all_tasks->result();


                
                //Sajár Maga Által Generált Task Lekérés
                $own_tasks = $this->db->query("SELECT COUNT(id) AS owntask FROM tasks WHERE automation_id IS NULL AND organization = '$organization' AND created_date BETWEEN '2018-$fomatted_i-01 00:00:01' AND '2018-$fomatted_i-$lastDayThisMonth 23:59:59' ORDER BY organization ASC");
                $own_tasks->result();
                
                //Összes Elvégzett Task
                $done_tasks = $this->db->query("SELECT COUNT(id) AS done_task FROM tasks WHERE status = '1' AND organization = '$organization' AND done_date BETWEEN '2018-$fomatted_i-01 00:00:01' AND '2018-$fomatted_i-31 23:59:59' ORDER BY organization ASC");
                $done_tasks->result();
                
                //Összes Elvégzett Task Automata Nélkül
                $done_tasks_own = $this->db->query("SELECT COUNT(id) AS done_task_own FROM tasks WHERE automation_id IS NULL AND status = '1' AND organization = '$organization' AND done_date BETWEEN '2018-$fomatted_i-01 00:00:01' AND '2018-$fomatted_i-31 23:59:59' ORDER BY organization ASC");
                $done_tasks_own->result();
                
                //Hívás Taskok
                $call_tasks = $this->db->query("SELECT COUNT(id) AS call_task FROM tasks WHERE task_type = '1' AND organization = '$organization' AND created_date BETWEEN '2018-$fomatted_i-01  00:00:01' AND '2018-$fomatted_i-31 23:59:59' ORDER BY organization ASC");
                $call_tasks->result();
                
                //Hívás Taskok
                $call_tasks_done = $this->db->query("SELECT COUNT(id) AS call_task_done FROM tasks WHERE status = '1' AND task_type = '1' AND organization = '$organization' AND done_date BETWEEN '2018-$fomatted_i-01 00:00:01' AND '2018-$fomatted_i-31 23:59:59' ORDER BY organization ASC");
                $call_tasks_done->result();
            
                $datas['month'][] = $i;
                $datas['organization'][] = $organization;
                $datas['organization_id'][] =  $organization_name['id'];
                $datas['organization_color'][] =  $organization_name['chart_color'];
                $datas['alltask'][] = $all_tasks->row('alltask');     
                $datas['owntask'][] = $own_tasks->row('owntask');
                $datas['donetask'][] = $done_tasks->row('done_task');
                $datas['done_task_own'][] = $done_tasks_own->row('done_task_own');
                $datas['calltask'][] = $call_tasks->row('call_task');
                $datas['calltask_done'][] = $call_tasks_done->row('call_task_done');

            }
            
        }
        $this->load->view('year_report_view', $datas);
    }

}

