<?php
    $i = 0;
    $organization_donetask_number=array();
?>
    
    <div class="row">
        <div class="col-lg-12">
            
            <div class="title">
            <br> 
            <h2 class="text-center">Elvégzett Taskok 2018-ban</h2>
            </div>
     
            <div class="table-responsive table--no-card m-b-30">
                <table class="table report-table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th class="text-center">Név</th>
                            <?php for ($m = 1; $m <= 12; $m++) { ?>
                                <th class="text-center"><?php echo $m; ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td class="text-center"><?php echo $organization[$i]; ?></td>
                        <?php 

                        $max = count($organization);
                        $organization_name = array();
                        $organization_chart_color = array();

                        array_push($organization_name, $organization[$i]);
                        array_push($organization_chart_color, $organization_color[$i]);
                        
                        foreach ($month as $months) {  
                            $current_organization = $organization[$i];
                              
                        ?>
                            <td class="text-center"><?php echo $donetask[$i]; ?></td>
                            <?php array_push($organization_donetask_number, $donetask[$i]); ?>
                        <?php 
                            $i++;
                           
                            if ($i <= $max-1) {
                                
                                $next_organization = $organization[$i];

                                if ($current_organization !== $next_organization) {

                                    
                                    array_push($organization_name, $organization[$i]);
                                    array_push($organization_chart_color, $organization_color[$i]);

                                    echo "</tr><tr>"; ?>
                                    <td class="text-center"><?php echo $organization[$i]; ?></td>
                            <?php  } 

                            }
                                     
                        }
                        unset($i);
                        $i = 0;
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <h3 class="title-2 m-b-40">Elvégzett Taskok 2018-ban</h3>
                    <canvas id="yearly-donetask-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <script>
    try {
    //Sales chart
    var ctx = document.getElementById("yearly-donetask-chart");
    if (ctx) {
      ctx.height = 100;
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
          type: 'line',
          defaultFontFamily: 'Open Sans',
          datasets: [
          <?php 
            $k = 0;
            $organization_donetask_numbers = array_chunk($organization_donetask_number, 12);
                foreach ($organization_donetask_numbers as $done_task_numbers) { 
                    
                    ?>{
            label: "<?php echo $organization_name[$k];?>",
            data:[<?php
                    foreach ($done_task_numbers as $done_task_number) {
                        echo $done_task_number.',';
                        
            }  ?>],
            backgroundColor: 'transparent',
            borderColor: '<?php echo $organization_chart_color[$k];?>',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: '<?php echo $organization_chart_color[$k];?>',
          }, 
          <?php $k++;  } ?>]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Open Sans',
            bodyFontFamily: 'Open Sans',
            cornerRadius: 3,
            intersect: false,
          },
          legend: {
            display: false,
            labels: {
              usePointStyle: true,
              fontFamily: 'Open Sans',
            },
          },
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: false,
                labelString: 'Month'
              },
              ticks: {
                fontFamily: "Open Sans"
              }
            }],
            yAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              scaleLabel: {
                display: true,
                labelString: 'Value',
                fontFamily: "Open Sans"

              },
              ticks: {
                fontFamily: "Open Sans"
              }
            }]
          },
          title: {
            display: false,
            text: 'Normal Legend'
          }
        }
      });
    }


  } catch (error) {
    console.log(error);
  }
</script>
