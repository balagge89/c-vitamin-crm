<?php
    defined('BASEPATH') or exit('No direct script access allowed'); 
    
    //Load Header
    $this->load->view('header');
    
    //Load Content
    $this->load->view('reports/year_alltask_report');
    $this->load->view('reports/year_owntask_report');
    $this->load->view('reports/year_donetask_report');
    $this->load->view('reports/year_done_task_own_report');
    $this->load->view('reports/year_call_task_report');
    $this->load->view('reports/year_call_task_done_report');
    
    //Load Footer
    $this->load->view('footer'); 
?>