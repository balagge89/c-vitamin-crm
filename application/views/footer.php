
                </div>
            </div>
        </div>
           <!-- Vendor JS       -->
    <script src="<?php echo base_url('bootstrap/vendor/slick/slick.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/wow/wow.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/animsition/animsition.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/counter-up/jquery.waypoints.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/counter-up/jquery.counterup.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/circle-progress/circle-progress.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/perfect-scrollbar/perfect-scrollbar.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/chartjs/Chart.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/select2/select2.min.js');?>"></script>
        <!-- Main JS -->
    <script src="<?php echo base_url('bootstrap/js/main.js');?>"></script>
</html>