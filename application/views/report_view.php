<?php
    defined('BASEPATH') or exit('No direct script access allowed'); 
    $this->load->view('header');
    
    if ( (empty($organization)) || (empty($start_date)) || (empty($end_date)) ) {
        $organization = NULL;
        $start_date = NULL;
        $end_date = NULL;
        $alltask = NULL;
        $owntask = NULL;
        $done_task = NULL;
        $done_task_own = NULL;
        $call_task = NULL;
        $call_task_done = NULL;
        $brochure_date_diff_avg = NULL;
        $reach_date_diff_avg = NULL;
        $open_status_patient = NULL;
        $open_status_patient_with_town = NULL;
        $brochure_sent_15_days_percent = NULL;
        $user_tags_data = NULL;
        $user_lists_data = NULL;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <div class="title">
            <h1>C-Vitamin Értékesítői Statisztika</h1>
        </div>
        <div class="card monthly-stats">
            <div class="card-header">
                <strong>Havi</strong> Statisztika
            </div>
            <div class="card-body card-block">
            <?php echo form_open('report/query'); ?>
                    <div class="form-group">
                        <span>Kezdő Dátum: <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az időintervallum első napja"></i></span><input class="form-control" type="date" value="<?php echo $start_date; ?>" name="start_date" required>
                        <span>Záró Dátum: <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az időintervallum utolsó napja"></i></span><input class="form-control" type="date" value="<?php echo $end_date; ?>" name="end_date" required>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Lekérdezés</button>
                    </div>
                    
                    <?php echo form_close(); ?>
            </div> <!-- end card-body -->
        </div> <!-- end card -->
    </div><!-- end column -->
</div><!-- end row -->
<?php if ( (!empty($organization)) || (!empty($start_date)) || (!empty($end_date)) ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="title">         
                <h2>Intervallum: <?php echo $start_date; ?> - <?php echo $end_date; ?></h2>
            </div>

            <?php 
            $i = 0;
            ?>
            <div class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th class="text-center">Név</th>
                            <th class="text-center">Összes Task <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az összes task amit az értékesítő az adott időintervallumon belül kapott (Automaták és Saját maguk által generáltakkal együtt)."></i></th>
                            <th class="text-center">Saját Maga Által Generált Task <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül kapott összes task közül ennyi volt a saját maguk által generált."></i></th>
                            <th class="text-center">Elvégzett Taskok / Sajátok <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül elvégezettnek jelölt feladat. (A feladat létrejöhetett az időintervallumon kívül is). És ebből mennyi volt az amit előzőleg saját maga generált"></i></th>
                            <th class="text-center">Hívás Taskok / Elvégzett <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül generálódott Hívás taskok (Automaták és Saját maguk által generáltakkal együtt). / Az adott időintervallumon belül elvégezettnek jelölt Hívás taskok (A task generálódhatott az időintervallumon kívül is)."></i></th>
                            <th class="text-center">Prosi átl. kiküldése <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül beérkezett leadeknek átlagosan ennyi idő alatt kerül feladásra prospektus."></i></th>
                            <th class="text-center">Prosi kiküldve 15 nap alatt <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül beérkezett leadeknek ennyi százalékának lett 15 napon belül feladva prospektuk"></i></th>
                            <th class="text-center">Érdembeni elérések átlaga <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül beérkezett leadeket átlagosan ennyi idő éri el."></i></th>
                            <th class="text-center">Open Páciens / Város <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott időintervallumon belül beérkezett leadekből ennyi Open státuszúhoz van a város feltöltve"></i></th>
                            <th class="text-center">Tagek <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott intervallumom belül kiosztott tagek és azok száma"></i></th>
                            <th class="text-center">Listák <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="left" title="Az adott intervallumom belül listákra felvételek és azok száma"></i></th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($organization as $organization_name) {  ?>
                        <tr>
                            <td class="text-center"><?php echo $organization[$i]; ?></td>
                            <td class="text-center"><?php echo $alltask[$i]; ?></td>
                            <td class="text-center"><?php echo $owntask[$i]; ?></td>
                            <td class="text-center"><?php echo $done_task[$i]; ?> / <?php echo $done_task_own[$i]; ?></td>
                            <td class="text-center"><?php echo $call_task[$i]; ?> / <?php echo $call_task_done[$i]; ?></td>
                            <td class="text-center"><?php echo $brochure_date_diff_avg[$i]; ?> Nap</td>
                            <td class="text-center"><?php echo $brochure_sent_15_days_percent[$i]; ?>%</td>
                            <td class="text-center"><?php echo $reach_date_diff_avg[$i]; ?> Nap</td>
                            <td class="text-center"><?php echo $open_status_patient[$i]; ?> / <?php echo $open_status_patient_with_town[$i]; ?> </td>
                            <td class="text-center">
                                    <?php 
                                  
                                    $array_count = count($user_tags_data[$i]);
                                    for ($j = 0; $j <= $array_count-1; $j++) {
                                        echo $user_tags_data[$i][$j]->tag.": ".$user_tags_data[$i][$j]->users."<br>";
                                    }
                                    ?>
                            </td>
                            <td class="text-center">
                                    <?php 
                                  
                                    $array_count_list = count($user_lists_data[$i]);
                                    for ($k = 0; $k <= $array_count_list-1; $k++) {
                                        echo $user_lists_data[$i][$k]->list.": ".$user_lists_data[$i][$k]->users."<br>";
                                    }
                                    ?>
                            </td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
<?php  }
$this->load->view('footer'); ?>