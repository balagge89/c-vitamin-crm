
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>C-Vitamin Reports</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url('bootstrap/'); ?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url('bootstrap/'); ?>css/theme.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url('bootstrap/'); ?>css/custom.css" rel="stylesheet" media="all">

        <!-- Jquery JS-->
        <script src="<?php echo base_url('bootstrap/vendor/jquery-3.2.1.min.js'); ?>"></script>
        <!-- Bootstrap JS-->
    <script src="<?php echo base_url('bootstrap/vendor/bootstrap-4.1/popper.min.js');?>"></script>
    <script src="<?php echo base_url('bootstrap/vendor/bootstrap-4.1/bootstrap.min.js');?>"></script>
 
    </body>
</head>
<body>
  <div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">